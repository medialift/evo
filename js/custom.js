
$( document ).ready(function() {
  jQuery.fn.highlight = function (str, className) {
      var regex = new RegExp(str, "gi");

      return this.each(function () {
          this.innerHTML = this.innerHTML.replace(regex, function(matched) {return "<span class=\"" + className + "\">" + matched + "</span>";});
      });
  };

  jQuery.fn.highlight2 = function (str, className) {
      var regex2 = new RegExp(str, "gi");

      return this.each(function () {
          this.innerHTML = this.innerHTML.replace(regex2, function(matched) {return "<span class=\"" + className + "\">" + matched + "</span>";});
      });
  };

  jQuery.fn.highlight3 = function (str, className) {
      var regex3 = new RegExp(str, "gi");

      return this.each(function () {
          this.innerHTML = this.innerHTML.replace(regex3, function(matched) {return "<span class=\"" + className + "\">" + matched + "</span>";});
      });
  };

  jQuery.fn.highlight4 = function (str, className) {
      var regex4 = new RegExp(str, "gi");

      return this.each(function () {
          this.innerHTML = this.innerHTML.replace(regex4, function(matched) {return "<span class=\"" + className + "\">" + matched + "</span>";});
      });
  };

  jQuery.fn.highlight5 = function (str, className) {
      var regex5 = new RegExp(str, "gi");

      return this.each(function () {
          this.innerHTML = this.innerHTML.replace(regex5, function(matched) {return "<span class=\"" + className + "\">" + matched + "</span>";});
      });
  };

  jQuery.fn.highlight7 = function (str, className) {
      var regex7 = new RegExp(str, "gi");

      return this.each(function () {
          this.innerHTML = this.innerHTML.replace(regex7, function(matched) {return "<span class=\"" + className + "\">" + matched + "</span>";});
      });
  };

  $("span.underline").highlight("[pgqjfy]","highlight");
  $("span.underline2").highlight2("[pgqjfy]","highlight");
  $("span.underline3").highlight3("[pgqjfy]","highlight");
  $("span.underline4").highlight4("[pgqjfy]","highlight");
  $("span.underline5").highlight5("[pgqjfy]","highlight");
  $("span.underline7").highlight7("[pgqjfy]","highlight");
});
